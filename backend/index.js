const express = require('express');
const app = express();
const port = process.env.PORT || 3000;
const bodyParser =  require('body-parser');
const { MonedaRoute, PerfilRoute, LenguajeRoute,PaisRoute } = require('./routes');
const cors = require('cors')
// CONEXION
const mongoose = require('mongoose');
const { db } = require('./config');
mongoose.connect(db.urlConexion, { useNewUrlParser: true, useUnifiedTopology: true });
app.use(cors())

app.use(bodyParser.json());

app.use(PerfilRoute);
app.use(MonedaRoute);
app.use(LenguajeRoute);
app.use(PaisRoute);


app.listen(port, () => {
  console.log(`===> Funcionando en el puerto ${port}`);
});
