module.exports = {
  PerfilController: require('./PerfilController'),
  MonedaController: require('./MonedaController'),
  LenguajeController: require('./LenguajeController'),
  PaisController: require('./PaisController'),
};