const { app, constants } = require('../config');
const { MonedaModel } = require('../models');
const axios = require('axios');

const findAll = async (req, res) => {
  const registros = await MonedaModel.find();
  res.status(200).json({
    finalizado: true,
    mensaje: 'Listado correctamente.',
    datos: registros
  });
};

const create = async (req, res) => {
  const datos = req.body;
  const registroCreado = await MonedaModel.create(datos);
  res.status(201).json({
    finalizado: true,
    mensaje: 'Registro creado correctamente.',
    datos: registroCreado
  });
};

const update = async (req, res) => {
  const { _id } = req.params;
  const datos = req.body;
  const registroActualizado = await MonedaModel.updateOne({ _id }, { $set: datos });
  res.status(200).json({
    finalizado: true,
    mensaje: 'Registro actualizado correctamente.',
    datos: registroActualizado
  });
};

const findById = async (req, res) => {
  const { _id } = req.params;
  const registroBuscado = await MonedaModel.findById(_id);
  res.status(200).json({
    finalizado: true,
    mensaje: 'Registro listado correctamente.',
    datos: registroBuscado
  });
};



const deleteItem = async (req, res) => {
  const { _id } = req.params;
  const registroEliminado = await MonedaModel.deleteOne({ _id });
  res.status(200).json({
    finalizado: true,
    mensaje: 'Registro eliminado correctamente.',
    datos: registroEliminado
  });
};


module.exports = {
  findAll,
  create,
  update,
  findById,
  deleteItem
};