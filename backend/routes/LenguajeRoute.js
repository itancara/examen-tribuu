const express = require('express');
const router = express.Router();
const { LenguajeController } = require('../controllers');

router.get('/lenguaje', LenguajeController.findAll);
router.get('/lenguaje/:_id', LenguajeController.findById);
router.post('/lenguaje', LenguajeController.create);
router.put('/lenguaje/:_id', LenguajeController.update);
router.delete('/lenguaje/:_id', LenguajeController.deleteItem);


module.exports = router;