module.exports = {
  PerfilRoute: require('./PerfilRoute'),
  MonedaRoute: require('./MonedaRoute'),
  LenguajeRoute: require('./LenguajeRoute'),
  PaisRoute: require('./PaisRoute'),
};