const express = require('express');
const router = express.Router();
const { MonedaController } = require('../controllers');

router.get('/moneda', MonedaController.findAll);
router.get('/moneda/:_id', MonedaController.findById);
router.post('/moneda', MonedaController.create);
router.put('/moneda/:_id', MonedaController.update);
router.delete('/moneda/:_id', MonedaController.deleteItem);


module.exports = router;