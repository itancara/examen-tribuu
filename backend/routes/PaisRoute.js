const express = require('express');
const router = express.Router();
const { PaisController } = require('../controllers');

router.get('/pais', PaisController.findAll);
router.get('/pais/:_id', PaisController.findById);
router.post('/pais', PaisController.create);
router.put('/pais/:_id', PaisController.update);
router.delete('/pais/:_id', PaisController.deleteItem);


module.exports = router;