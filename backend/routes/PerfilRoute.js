const express = require('express');
const router = express.Router();
const { PerfilController } = require('../controllers');

router.get('/perfil', PerfilController.findAll);
router.get('/perfil/:_id', PerfilController.findById);
router.post('/perfil', PerfilController.create);
router.put('/perfil/:_id', PerfilController.update);
router.delete('/perfil/:_id', PerfilController.deleteItem);


module.exports = router;