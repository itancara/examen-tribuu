const mongoose = require('mongoose');
const { Schema } = mongoose;

const PerfilSchema = new Schema({
  role: String,
  language: Array,
  email: String,
  name: String,
  lastname: String,
  currency: Object,
  country: String,
  countryCode: String,
  countryCodeName: String,
  phone: String
});

const Perfil = mongoose.model('perfil', PerfilSchema);

module.exports = Perfil;
