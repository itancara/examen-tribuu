module.exports = {
  PerfilModel: require('./PerfilModel'),
  MonedaModel: require('./MonedaModel'),
  LenguajeModel: require('./LenguajeModel'),
  PaisModel: require('./PaisModel'),
};