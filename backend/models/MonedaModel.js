const mongoose = require('mongoose');
const { Schema } = mongoose;

const ModenaSchema = new Schema({
  code: String,
  name: String,
  symbol: String,
});

const Moneda = mongoose.model('moneda', ModenaSchema);

module.exports = Moneda;
