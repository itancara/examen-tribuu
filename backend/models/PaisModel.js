const mongoose = require('mongoose');
const { Schema } = mongoose;

const PaisSchema = new Schema({
  nombre: String
});

const Pais = mongoose.model('pais', PaisSchema);

module.exports = Pais;
