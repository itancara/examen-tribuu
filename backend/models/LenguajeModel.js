const mongoose = require('mongoose');
const { Schema } = mongoose;

const LenguajeSchema = new Schema({
  nombre: String
});

const Lenguaje = mongoose.model('lenguaje', LenguajeSchema);

module.exports = Lenguaje;
