import { NgModule } from '@angular/core';

import { WelcomeRoutingModule } from './welcome-routing.module';

import { WelcomeComponent } from './welcome.component';
import { NzFormModule } from 'ng-zorro-antd/form';
import { NzInputModule } from 'ng-zorro-antd/input';
import { ReactiveFormsModule } from '@angular/forms';
import { NzButtonModule } from 'ng-zorro-antd/button'
import { NzSelectModule } from 'ng-zorro-antd/select'
import { CommonModule } from '@angular/common';
import { NzPageHeaderModule } from 'ng-zorro-antd/page-header';
import { NzDividerModule } from 'ng-zorro-antd/divider';
import { HttpClientModule } from '@angular/common/http';
import { WelcomeService } from './welcome.service';

@NgModule({
  imports: [
    HttpClientModule,
    NzDividerModule,
    NzPageHeaderModule,
    CommonModule,
    NzSelectModule,
    NzButtonModule,
    ReactiveFormsModule,
    WelcomeRoutingModule,
    NzFormModule,
    NzInputModule
  ],
  providers: [WelcomeService],
  declarations: [WelcomeComponent],
  exports: [WelcomeComponent]
})
export class WelcomeModule { }
