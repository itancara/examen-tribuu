import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment'

@Injectable({
  providedIn: 'root'
})
export class WelcomeService {
  url: string = environment.BACKEND_URL

  constructor(private _httpClient: HttpClient) { }

  async getMonedas(): Promise<any> {
    const respuesta = await this._httpClient.get(`${this.url}/moneda`).toPromise()
    return respuesta;
  }

  async getLenguajes(): Promise<any> {
    const respuesta = await this._httpClient.get(`${this.url}/lenguaje`).toPromise()
    return respuesta;
  }

  async getPaises(): Promise<any> {
    const respuesta = await this._httpClient.get(`${this.url}/pais`).toPromise()
    return respuesta;
  }

  async getProfile(id: string): Promise<any> {
    const respuesta = await this._httpClient.get(`${this.url}/perfil/${id}`).toPromise()
    return respuesta;
  }

  async saveProfile(id: string, datos: any): Promise<any> {
    let respuesta = null;
    if (id) {
      respuesta = await this._httpClient.put(`${this.url}/perfil/${id}`, datos).toPromise()
    } else {
      respuesta = await this._httpClient.post(`${this.url}/perfil`, datos).toPromise()
    }
    return respuesta;
  }
}
