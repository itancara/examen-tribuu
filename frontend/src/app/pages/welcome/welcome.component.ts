import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { WelcomeService } from './welcome.service';

@Component({
  selector: 'app-welcome',
  templateUrl: './welcome.component.html',
  styleUrls: ['./welcome.component.less']
})
export class WelcomeComponent implements OnInit {

  validateForm!: FormGroup;
  lenguajes: Array<any> = [];

  monedas: Array<any> = [];

  paises: Array<any> = [];

  id: string = '61768120e270e90df071918f';

  async submitForm() {
    for (const i in this.validateForm.controls) {
      if (this.validateForm.controls.hasOwnProperty(i)) {
        this.validateForm.controls[i].markAsDirty();
        this.validateForm.controls[i].updateValueAndValidity();
      }
    }
    const datos = this.mapToSend()
    const respuesta = await this._welcomeService.saveProfile(this.id, datos);
  }

  constructor(private fb: FormBuilder, private _welcomeService: WelcomeService) {
    this.validateForm = this.fb.group({
      role: [null, [Validators.required]],
      language: [null, [Validators.required]],
      email: [null, [Validators.required, Validators.email]],
      name: [null, [Validators.required]],
      lastname: [null, [Validators.required]],
      currency: [null, [Validators.required]],
      country: [null, [Validators.required]],
      countryCode: [null, [Validators.required]],
      countryCodeName: [null, [Validators.required]],
      phone: [null, [Validators.required]]
    });
  }


  async ngOnInit(): Promise<any> {
    const { datos: monedas } = await this._welcomeService.getMonedas();
    this.monedas = monedas;
    const { datos: lenguajes } = await this._welcomeService.getLenguajes();
    this.lenguajes = lenguajes;
    const { datos: paises } = await this._welcomeService.getPaises();
    this.paises = paises;
    if (this.id) {
      const { datos: profile } = await this._welcomeService.getProfile(this.id)
      this.mapData(profile)
    }
  }

  mapToSend() {
    const moneda = this.monedas.find(x => x._id === this.validateForm.get('currency')?.value)
    return {
      role: this.validateForm.get('role')?.value,
      language: this.validateForm.get('language')?.value,
      email: this.validateForm.get('email')?.value,
      name: this.validateForm.get('name')?.value,
      lastname: this.validateForm.get('lastname')?.value,
      currency: moneda,
      country: this.validateForm.get('country')?.value,
      countryCode: this.validateForm.get('countryCode')?.value,
      countryCodeName: this.validateForm.get('countryCodeName')?.value,
      phone: this.validateForm.get('phone')?.value
    }
  }

  mapData(profile: any) {
    this.validateForm.get('role')?.setValue(profile.role)
    this.validateForm.get('language')?.setValue(profile.language)
    this.validateForm.get('email')?.setValue(profile.email)
    this.validateForm.get('name')?.setValue(profile.name)
    this.validateForm.get('lastname')?.setValue(profile.lastname)
    this.validateForm.get('currency')?.setValue(profile.currency._id)
    this.validateForm.get('country')?.setValue(profile.country)
    this.validateForm.get('countryCode')?.setValue(profile.countryCode)
    this.validateForm.get('countryCodeName')?.setValue(profile.countryCodeName)
    this.validateForm.get('phone')?.setValue(profile.phone)
    console.log('====================_MAP_DATA_====================')
    console.log(profile)
    console.log('====================_MAP_DATA_====================')
  }

  cancelar() {
    console.log('====================_MENSAJE_A_MOSTRARSE_====================')
    console.log('cancelar')
    console.log('====================_MENSAJE_A_MOSTRARSE_====================')
  }
}