# FRONTEND
## 1. Instalar las dependencias con el comando:
```
yarn install
```

## 2. Levantar el servidor de Frontend on el comando.
```
ng serve
```

# BACKEND
## 1. Instalar las dependencias con el comando:
```
yarn install
```

## 2. Instalar la dependencia global nodemon con:
```
  npm i -g nodemon
```

## 3. Levantar el servidor de Nodejs con el comando:
```
  yarn start:dev
```